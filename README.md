# Arduino based zoom controller #

using arduino pro (pro micro / leonardo copy).

have conntect 4 (mechanical keys) buttons to pins and gnd.

each button will control zoom with the following shortcuts


  * Left Alt + 'v' - start/stop video
  * Left Alt + 'n' - switch cameras
  * Left Alt + 'a' - mute host (own) mic
  * Left Alt + 'm' - mute all except host
  
I'm using  [Nico Hood's hid-project](https://github.com/NicoHood/HID)
which extends the arduino  [Keyboard
library](https://www.arduino.cc/reference/en/language/functions/usb/keyboard/).

I make sure I've got the leonardo avr board installed for the
arduino-cli and that Nico Hood's hid-project is installed with
`arduino-cli lib install hid-project`. 

Then Compile

```
arduino-cli compile -b arduino:avr:leonardo CameraSwitcher
```

and then upload

```
arduino-cli upload -p /dev/ttyACM0 --fqbn arduino:avr:leonardo CameraSwitcher

```

(note - it might take a few attempts to send to pro micro as first
attempt might not wait for bootloader to start).
