/*

  Bekka's Zoom Keys
  Version 1.0
  08/01/2020

  Author:
  Tom Broughton
  
  Dependencies:
  HID-Project by NicoHood Version 2.5.0 or above
  - can be found in Arduino Lirary Manager
  - or: https://github.com/NicoHood/HID

*/
// include the HID library
#include "HID-Project.h"
const int numButtons = 4;
// definitions for each pin used
const int toggleVideoPin = 9; //turn local Video on/off
const int toggleCameraPin = A0; //switch cameras
const int toggleMicPin = 10; //turn on/off local mic
const int muteAllRemotesPin = 8; //mute all participant's

const char toggleVideoVal = 'v';
const char toggleCameraVal = 'n';
const char toggleMicVal = 'a';
const char muteAllRemotesVal = 'm';

int btnPins[numButtons] = {toggleVideoPin, toggleCameraPin, toggleMicPin, muteAllRemotesPin};
int btnVals[numButtons] = {toggleVideoVal, toggleCameraVal, toggleMicVal, muteAllRemotesVal};

int lastKeyPressed = 0;

void setup() {
  for(byte i=0; i<numButtons; i++){
    pinMode(btnPins[i], INPUT_PULLUP);
  }
  // begin HID connection
  Keyboard.begin();
}

void checkBtnPress(int pin, char val){
  if (!digitalRead(pin) && pin != lastKeyPressed) {
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.press(val);
    delay(250);
    lastKeyPressed = pin;
  }
}

void checkBtnRelease(int pin, char val){
  if (pin == lastKeyPressed && digitalRead(pin)){
    Keyboard.release(val);
    Keyboard.release(KEY_LEFT_ALT);
    delay(250);
    lastKeyPressed = 0;
  }
}
  
void loop() {
  for(byte i=0; i<numButtons; i++){
    checkBtnPress(btnPins[i], btnVals[i]);
  }
  for(byte i=0; i<numButtons; i++){
    checkBtnRelease(btnPins[i], btnVals[i]);
  }
}
